"""Генерирует матрицу смежности графа!"""
import random

matrix_size = 1000
zero_probability_promille = 998


def random_of_ranges(*in_ranges):
    ranges = [list(n) for n in in_ranges]
    all_ranges = sum(ranges, [])
    return random.choice(all_ranges)


matrix = []
for x in range(matrix_size):
    row = []
    for y in range(matrix_size):
        if random.randrange(0, 1000) >= 1000 - zero_probability_promille:
            row.append(0)
        else:
            rand_num = random_of_ranges(range(0x30, 0x39), range(0x41, 0x5A), range(0x61, 0x7A))
            row.append(rand_num)
    matrix.append(row)

# Делает из дуг ребра
for i in range(matrix_size):
    for j in range(matrix_size):
        if matrix[i][j] != matrix[j][i]:
            matrix[i][j] = matrix[j][i]

# Убирает петли
for i in range(matrix_size):
    matrix[i][i] = 0

with open('matrix.csv', 'wt', encoding='utf8') as f:
    f.write(',')
    for i in range(matrix_size):
        f.write(f'A{i + 1}{"," if i != matrix_size - 1 else ""}')
    f.write('\n')
    for i in range(matrix_size):
        f.write(f'A{i + 1},')
        for j in range(matrix_size):
            if j == matrix_size - 1:
                f.write(str(matrix[i][j]))
            else:
                f.write(str(matrix[i][j]) + ',')
        f.write('\n')
