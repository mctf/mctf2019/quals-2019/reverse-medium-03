"""Перекодирует набор чисел в символы ASCII."""

string = ""
for item in "103 102 79 101 104 54 65 119 87 105 55 51 112 100 55 99 104 77 120 100".split(' '):
    string += chr(int(item))

print(string)
